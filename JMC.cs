using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class JMC : MonoBehaviour
{
    [HideInInspector]
    public float globalTweenTime = 0.5f;

    /// <summary>
    /// Check if we are at Game Over
    /// </summary>
    /// <param name="lives"></param>
    /// <returns></returns>
    public bool IsGameOver(int lives)
    {
        return lives == 0;
    }

    /// <summary>
    /// Works out the change in percentage between two scores
    /// </summary>
    /// <param name="scoreOne">The first score</param>
    /// <param name="scoreTwo">The second score</param>
    /// <returns></returns>
   
    public static float PercentageChange(float scoreOne, float scoreTwo)
    {
        float change = scoreTwo - scoreOne;
        return change / scoreOne * 100;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
